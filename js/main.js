;(function () {
	'use strict';

	var $ = TweenMax,
		bg = document.getElementById('coffee'),
		tlCopy = new TimelineMax(),
		containerSize = document.getElementsByClassName("containerSize"),
		plus = document.getElementsByClassName("plus");

		window.init = function() {


			for (var i = 0; i < containerSize.length; i++) {
    			containerSize[i].addEventListener('click', playAnim, false);
			}

			for (var i = 0; i < plus.length; i++) {
    			plus[i].addEventListener('click', makeAdition, false);
			}
			
			function makeAdition(e){
				
				var element = document.getElementsByClassName(e);
					console.log(element);
   					plus.classList.addClass("mystyle");
			}

			function playAnim(e){

				var els = Array.prototype.slice.call(document.getElementsByClassName("containerSize"), 0 ),
					classNum = els.indexOf(event.currentTarget);

					for (var i = 0; i < containerSize.length; i++) {

						if(i == classNum){
							$.to(containerSize[classNum],.4,{height:"52vh", ease:Sine.easeOut});
							$.to(containerSize[classNum].children[0],.4,{height:"12vh", ease:Sine.easeOut});
							$.to(containerSize[classNum].children[1],.4,{height:"12vh", ease:Sine.easeOut});
						}

						if(i>classNum){
							$.to(containerSize[i],.4,{height:"12vh", ease:Sine.easeOut});
							$.to(containerSize[i].children[0],.4,{height:"100%", ease:Sine.easeOut});
							$.to(containerSize[i].children[1],.4,{height:"100%", ease:Sine.easeOut});
						}

						if(i<classNum){
							$.to(containerSize[i],.4,{height:"12vh", ease:Sine.easeOut});
							$.to(containerSize[i].children[0],.4,{height:"100%", ease:Sine.easeOut});
							$.to(containerSize[i].children[1],.4,{height:"100%", ease:Sine.easeOut});
						}
						
					}

			} 

			//$.to('.Sweet',.7,{height:"50%", ease:Sine.easeOut},3)
			
		}
		
		init();

}());